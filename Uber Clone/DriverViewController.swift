//
//  DriverViewController.swift
//  Uber Clone
//
//  Created by kerimcaglar on 24/12/2017.
//  Copyright © 2017 kerimcaglar. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseDatabase
import MapKit

class DriverViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, CLLocationManagerDelegate {

    @IBOutlet weak var tableView: UITableView!
    var taksiCagiranlar : [DataSnapshot] = []
    var konumYoneticisi = CLLocationManager()
    var soforKonum = CLLocationCoordinate2D()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        konumYoneticisi.delegate = self
        konumYoneticisi.desiredAccuracy = kCLLocationAccuracyBest
        konumYoneticisi.requestWhenInUseAuthorization()
        konumYoneticisi.startUpdatingLocation()
        
        Database.database().reference().child("TaksiCagiranlar").observe(.childAdded) { (snapshot) in
            self.taksiCagiranlar.append(snapshot)
            self.tableView.reloadData()
        }
        
        Timer.scheduledTimer(withTimeInterval: 5, repeats: true) { (updateTimer) in
            self.tableView.reloadData()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let soforKoord = manager.location?.coordinate{
            soforKonum = soforKoord
        }
    }

    @IBAction func logout(_ sender: Any) {
        print("tıklandı")
        do {
            try Auth.auth().signOut()
            self.dismiss(animated: true, completion: nil)
            
        } catch  {
            print("Çıkış yapılamadı")
        }
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return taksiCagiranlar.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "taksiCagiranlarCell", for: indexPath)
        
        let snapshot = taksiCagiranlar[indexPath.row]
        
        if let taksiCagiranlarVerisi = snapshot.value as? [String:AnyObject]{
            if let email = taksiCagiranlarVerisi["email"] as? String{
                
                if let enlem = taksiCagiranlarVerisi["enlem"] as? Double{
                    if let boylam = taksiCagiranlarVerisi["boylam"] as? Double{
                        
                        let soforKonumBilgileri = CLLocation(latitude: soforKonum.latitude, longitude: soforKonum.longitude)
                        
                        let yolcuKonumBilgileri = CLLocation(latitude: enlem, longitude: boylam)
                        
                        let uzaklik = soforKonumBilgileri.distance(from: yolcuKonumBilgileri) / 1000
                        
                        let yaklasikUzaklik = round(uzaklik*100)/100
                        
                        cell.textLabel?.text = "\(email) - \(yaklasikUzaklik) mesafede "

                    }
                    
                }
                
               
            }
        }
        
        return cell
    }
    

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let stoaryboard = UIStoryboard(name: "Main", bundle: nil)
        let passengerVC = stoaryboard.instantiateViewController(withIdentifier: "TaxiRequestVC") as! TaxiRequestAcceptViewController
        let snapshot = taksiCagiranlar[indexPath.row]
        if let taxiRequestData = snapshot.value as? [String:AnyObject]{
            if let email = taxiRequestData["email"] as? String{
                if let enlem = taxiRequestData["enlem"] as? Double{
                    if let boylam = taxiRequestData["boylam"] as? Double{
                        passengerVC.userEmail = email
                        passengerVC.latitude = enlem
                        passengerVC.longitude = boylam
                    }
                }
            }
        }
        
        self.present(passengerVC, animated: true, completion: nil)
    }

}
