//
//  Helper.swift
//  Uber Clone
//
//  Created by kerimcaglar on 29/10/2017.
//  Copyright © 2017 kerimcaglar. All rights reserved.
//

import UIKit

extension ViewController{
    func showAlert(title:String, message:String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Tamam", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}

