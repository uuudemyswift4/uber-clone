//
//  TaxiRequestAcceptViewController.swift
//  Uber Clone
//
//  Created by kerimcaglar on 3.06.2018.
//  Copyright © 2018 kerimcaglar. All rights reserved.
//

import UIKit
import MapKit

class TaxiRequestAcceptViewController: UIViewController {
    
    @IBOutlet weak var requestMap: MKMapView!
    var userEmail:String?
    var latitude:Double?
    var longitude:Double?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        DispatchQueue.main.async {
            if let lat = self.latitude{
                if let lon = self.longitude{
                    let center = CLLocationCoordinate2DMake(lat, lon)
                    let span = MKCoordinateSpanMake(0.01, 0.01)
                    let region = MKCoordinateRegion(center: center, span: span)
                    self.requestMap.setRegion(region, animated: true)
                    let annotation = MKPointAnnotation()
                    annotation.coordinate = center
                    if let email = self.userEmail{
                        print("email ne geliyor:\(email)")
                        annotation.title = self.userEmail
                    }
                    
                    self.requestMap.addAnnotation(annotation)
                }
            }
        }
    }
    
    @IBAction func acceptRequest(_ sender: Any) {
    }
    
    @IBAction func back(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
