//
//  ViewController.swift
//  Uber Clone
//
//  Created by kerimcaglar on 18/09/2017.
//  Copyright © 2017 kerimcaglar. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseDatabase

class ViewController: UIViewController {

    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var choiceSegment: UISegmentedControl!
    
    var driver:Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    @IBAction func login(_ sender: Any) {
        if (email.text?.isEmpty)! && (password.text?.isEmpty)! {
            showAlert(title: "Hata", message: "Lütfen boş alan bırakmayınız")
        }else{
            Auth.auth().signIn(withEmail: email.text!, password: password.text!, completion: { (user, error) in
                if error != nil{
                    self.showAlert(title: "Hata", message: (error?.localizedDescription)!)
                }else{

                    if self.driver{
                        let stoaryboard = UIStoryboard(name: "Main", bundle: nil)
                        let passengerVC = stoaryboard.instantiateViewController(withIdentifier: "DriverVC")
                        
                        self.present(passengerVC, animated: true, completion: nil)
                        self.email.text = ""

                    }else{
                        let stoaryboard = UIStoryboard(name: "Main", bundle: nil)
                        let passengerVC = stoaryboard.instantiateViewController(withIdentifier: "PassengerVC")
                        
                        self.present(passengerVC, animated: true, completion: nil)
                        self.email.text = ""

                    }
                }
            })
        }
    }
    
    @IBAction func selectUserType(_ sender: Any) {
        if choiceSegment.selectedSegmentIndex == 0 {
            print("yolcu seçildi")
            driver = false
            if(!(email.text?.isEmpty)!){
                self.login(sender)
            }
        }else{
            print("sofor seçildi")
            driver = true
            if(!(email.text?.isEmpty)!){
                self.login(sender)
            }
        }
    }
    
}
