//
//  CallUberViewController.swift
//  Uber Clone
//
//  Created by kerimcaglar on 12/11/2017.
//  Copyright © 2017 kerimcaglar. All rights reserved.
//

import UIKit
import MapKit
import FirebaseDatabase
import FirebaseAuth

class CallUberViewController: UIViewController, CLLocationManagerDelegate {
    
    @IBOutlet weak var uberMap: MKMapView!
    var locationManager = CLLocationManager()
    var userLocation = CLLocationCoordinate2D()
    var taxiCalled:Bool = false
    
    @IBOutlet weak var taxiButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        
        if let email = Auth.auth().currentUser?.email{
            Database.database().reference().child("TaksiCagiranlar").queryOrdered(byChild: "email").queryEqual(toValue: email).observe(.childAdded, with: { (snapshot) in
                self.taxiCalled = true //taksi zaten çağırmış
                self.taxiButton.setTitle("İptal Et", for: .normal)
            })
        }
        
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let coord = manager.location?.coordinate{
            let center = CLLocationCoordinate2DMake(coord.latitude, coord.longitude)
            userLocation = center
            let span = MKCoordinateSpanMake(0.01, 0.01)
            let region = MKCoordinateRegion(center: center, span: span)
            uberMap.setRegion(region, animated: true)
            
            let annotation = MKPointAnnotation()
            annotation.coordinate = center
            annotation.title = "Buradasınız"
            
            uberMap.addAnnotation(annotation)
        }
    }
    
    @IBAction func callTaxi(_ sender: Any) {
        let email = Auth.auth().currentUser?.email
        let userInfos:[String:Any] = ["email":email!, "enlem":userLocation.latitude, "boylam":userLocation.longitude]

        if taxiCalled{
            taxiCalled = false
            taxiButton.setTitle("Taksi Çağır", for: .normal)
            //veritabanından taksi isteğini kaldırmalıyız
            Database.database().reference().child("TaksiCagiranlar").queryOrdered(byChild: "email").queryEqual(toValue: email).observe(.childAdded, with: { (snapshot) in
                snapshot.ref.removeValue()
                Database.database().reference().child("TaksiCagiranlar").removeAllObservers()
            })
        }else{
            Database.database().reference().child("TaksiCagiranlar").childByAutoId().setValue(userInfos)
            taxiButton.setTitle("İptal Et", for: .normal)
            taxiCalled = true
        }
        
    }
    
    @IBAction func logout(_ sender: Any) {
        do {
            try Auth.auth().signOut()
            self.dismiss(animated: true, completion: nil)
        } catch  {
            print("Çıkış yapılamadı")
        }
    }
    
}
